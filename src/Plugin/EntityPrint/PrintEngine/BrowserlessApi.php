<?php

namespace Drupal\entity_print_browserless_pdf\Plugin\EntityPrint\PrintEngine;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Http\ClientFactory;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\entity_print\Plugin\ExportTypeInterface;
use Drupal\entity_print\Plugin\EntityPrint\PrintEngine\PdfEngineBase;
use Drupal\entity_print\PrintEngineException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\PositiveOrZero;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Url as UrlConstraint;

/**
 * PHP wkhtmltopdf plugin.
 *
 * @PrintEngine(
 *   id = "browserless_api",
 *   label = @Translation("Browserless API"),
 *   export_type = "pdf"
 * )
 */
class BrowserlessApi extends PdfEngineBase implements ContainerFactoryPluginInterface {

  /**
   * Current Request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Module logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The Browserless API endpoint.
   *
   * @var \Drupal\Core\Url
   */
  protected $endpoint;

  /**
   * Http client to connect to Browserless API.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * An array of html strings that represent a page.
   *
   * @var array
   */
  protected array $pages = [];

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ExportTypeInterface $export_type, RequestStack $request_stack, ClientFactory $client_factory, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $export_type);
    $this->request = $request_stack->getCurrentRequest();
    $this->logger = $logger_factory->get('entity_print_browserless_pdf');

    // Build up the endpoint from the current configuration.
    $endpoint = Url::fromUri($this->configuration['endpoint'] . '/pdf');

    if ($this->configuration['token']) {
      $endpoint->setOptions([
        'query' => [
          'token' => $this->configuration['token'],
        ],
      ]);
    }

    $this->endpoint = $endpoint;

    $this->httpClient = $client_factory->fromOptions([
      'headers' => [
        'Content-Type' => 'application/json',
        'Cache-Control' => 'no-cache',
      ],
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.entity_print.export_type')->createInstance($plugin_definition['export_type']),
      $container->get('request_stack'),
      $container->get('http_client_factory'),
      $container->get('logger.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'endpoint' => 'https://chrome.browserless.io',
      'token' => NULL,
      'print_background' => FALSE,
      'safe_mode' => FALSE,
      'display_header_footer' => FALSE,
      'header_template' => NULL,
      'footer_template' => NULL,
      'asset_url' => NULL,
      'margin_unit' => 'mm',
      'margin_top' => '10',
      'margin_right' => '10',
      'margin_left' => '10',
      'margin_bottom' => '10',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    if ($this->testEndpoint() === 200) {
      $this->messenger()->addMessage($this->t('Successfully connected to Browserless API at @endpoint.', [
        '@endpoint' => $this->endpoint->toString(),
      ]));
    }

    $form['endpoint_debug'] = [
      '#markup' => 'Your current endpoint URL is ' . $this->endpoint->setAbsolute(TRUE)->toString(),
    ];

    $form['asset_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alternate asset URL'),
      '#description' => $this->t('asdf'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['asset_url'],
    ];

    $form['endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Custom API Endpoint'),
      '#description' => $this->t('Leave blank to use <a href="@paid_service_url" target="_blank">paid service</a> endpoint. This is useful if you are running your own Browserless instance. Include a port if using a port other than 80. (ie: https://example.com:3000)', [
        '@paid_service_url' => 'https://www.browserless.io/',
      ]),
      '#required' => FALSE,
      '#default_value' => $this->configuration['endpoint'],
    ];

    $form['token'] = [
      '#type' => 'password',
      '#title' => $this->t('Token'),
      '#description' => $this->t('Leave blank if you are not using a token to secure your Browserless instance. <strong>You should</strong>, however, secure your instance.'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['token'],
    ];

    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Chrome Print Options'),
    ];

    $form['options']['print_background'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Background Graphics'),
      '#description' => $this->t('Instruct the headless Chrome browser to print background graphics.'),
      '#default_value' => $this->configuration['print_background'],
    ];

    $form['options']['safe_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Safe Mode'),
      '#description' => $this->t('Prevent page render crashes in Headless Chrome. See <a href="@docs_url" target="_blank">documentation</a>.', [
        '@docs_url' => 'https://docs.browserless.io/docs/pdf.html#safemode',
      ]),
      '#default_value' => $this->configuration['safe_mode'],
    ];

    $form['options']['display_header_footer'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display Header and Footer'),
      '#description' => $this->t('Display header and footer. See <a href="@docs_url" target="_blank">documentation</a>.', [
        '@docs_url' => 'https://github.com/browserless/chrome/blob/master/src/schemas.ts#L196',
      ]),
      '#default_value' => $this->configuration['display_header_footer'],
    ];

    $form['options']['header_template'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Header Template'),
      '#description' => $this->t('HTML template to add a header on each page. Limitations include no external fonts, style sheets, or javascript. All styling must be done inline'),
      '#default_value' => $this->configuration['header_template'],
    ];

    $form['options']['footer_template'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Footer Template'),
      '#description' => $this->t('HTML template to add a footer on each page. Limitations include no external fonts, style sheets, or javascript. All styling must be done inline'),
      '#default_value' => $this->configuration['footer_template'],
    ];

    $form['options']['margins'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Margins'),
    ];

    $form['options']['margins']['margin_unit'] = [
      '#type' => 'select',
      '#title' => $this->t('Unit'),
      '#options' => [
        'mm' => $this->t('mm'),
        'px' => $this->t('px'),
      ],
      '#default_value' => $this->configuration['margin_unit'],
    ];

    $form['options']['margins']['margin_top'] = [
      '#type' => 'number',
      '#title' => $this->t('Margin Top'),
      '#step' => 1,
      '#default_value' => $this->configuration['margin_top'],
    ];

    $form['options']['margins']['margin_right'] = [
      '#type' => 'number',
      '#title' => $this->t('Margin Right'),
      '#step' => 1,
      '#default_value' => $this->configuration['margin_right'],
    ];

    $form['options']['margins']['margin_bottom'] = [
      '#type' => 'number',
      '#title' => $this->t('Margin Bottom'),
      '#step' => 1,
      '#default_value' => $this->configuration['margin_bottom'],
    ];

    $form['options']['margins']['margin_left'] = [
      '#type' => 'number',
      '#title' => $this->t('Margin Left'),
      '#step' => 1,
      '#default_value' => $this->configuration['margin_left'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {

    // Get the dompdf values.
    $values = $form_state->getValue('browserless_api');

    // Instantiate a validator.
    $validator = Validation::createValidator();
    // Build a trailing slash constraint.
    $trailing_slash = function (string $url_value, ExecutionContextInterface $context) {
      if (\str_ends_with($url_value, '/')) {
        $context->addViolation('The URL cannot end with a trailing slash.');
      }
    };
    // Get the constraint.
    $data_constraint = new Collection([
      'fields' => [
        'endpoint' => [
          // Is a valid URL.
          new UrlConstraint([
            'protocols' => ['http', 'https'],
          ]),
          // Does not end with a trailing slash.
          new Callback($trailing_slash),
        ],
        'token' => [
          new Type('string'),
        ],
        'asset_url' => [
          // Is a valid URL.
          new UrlConstraint([
            'protocols' => ['http', 'https'],
          ]),
          // Does not end with a trailing slash.
          new Callback($trailing_slash),
        ],
        'options' => new Collection([
          'margins' => new Collection([
            'margin_unit' => new Choice([
              'choices' => ['mm', 'px'],
              'message' => 'Choose a valid margin unit.',
            ]),
            'margin_top' => new PositiveOrZero(),
            'margin_right' => new PositiveOrZero(),
            'margin_bottom' => new PositiveOrZero(),
            'margin_left' => new PositiveOrZero(),
          ]),
          'safe_mode' => new Choice([0, 1]),
          'print_background' => new Choice([0, 1]),
          'display_header_footer' => new Choice([0, 1]),
          'header_template' => [
            new Type('string'),
          ],
          'footer_template' => [
            new Type('string'),
          ],
        ]),
      ],
      'allowMissingFields' => TRUE,
      'allowExtraFields' => TRUE,
    ]);

    /** @var \Symfony\Component\Validator\ConstraintViolationInterface[] $violations */
    $violations = $validator->validate($values, $data_constraint);

    // Map violation messages back to the field.
    if ($violations->count()) {
      foreach ($violations as $violation) {
        // Check the violation against the form values.
        $this->validateValues($values, $violation, $form_state);
      }
    }

    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * Validates a constraint violation against a form values array.
   *
   * @param array $values
   *   The form values.
   * @param \Symfony\Component\Validator\ConstraintViolation $violation
   *   The violation.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function validateValues(array $values, ConstraintViolation $violation, FormStateInterface $form_state) {
    $iterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($values), \RecursiveIteratorIterator::SELF_FIRST);
    // @codingStandardsIgnoreStart
    foreach ($iterator as $value) {
      // @codingStandardsIgnoreEnd
      // Only check the deepest values in the array.
      if (!$iterator->hasChildren()) {
        // Build a list of property and element paths.
        $element_path = [];
        $property_path = [];
        for ($i = 0, $depth = $iterator->getDepth(); $i <= $depth; $i++) {
          $property_path[] = '[' . $iterator->getSubIterator($i)->key() . ']';
          $element_path[] = $iterator->getSubIterator($i)->key();
        }

        $property_path = implode('', $property_path);
        if ($violation->getPropertyPath() === $property_path) {
          // Create the form element path.
          $element = implode('][', $element_path);
          // Set the form error.
          $form_state->setErrorByName('browserless_api][' . $element, $violation->getMessage());
        }
      }
    }
  }

  /**
   * Tests the Browserless endpoing by printing a dummy page.
   *
   * @return int|null
   *   Returns an HTTP status code or NULL upon connection failure.
   */
  private function testEndpoint() : ?int {

    try {
      $response = $this->httpClient->post($this->endpoint->toString(), [
        'json' => [
          'html' => '<html><body>Test</body></html>',
        ],
      ]);

      return $response->getStatusCode();
    }
    catch (ConnectException $e) {
      $this->messenger()->addError($e->getMessage());
      $this->logger->warning($e->getMessage());
      return NULL;
    }
    catch (ClientException $e) {
      $this->messenger()->addError($e->getMessage());
      $this->logger->warning($e->getMessage());
      return $e->getResponse()->getStatusCode();
    }
    catch (ServerException $e) {
      $this->messenger()->addError($e->getMessage());
      $this->logger->warning($e->getMessage());
      return $e->getResponse()->getStatusCode();
    }
    catch (BadResponseException $e) {
      $this->messenger()->addError($e->getMessage());
      $this->logger->warning($e->getMessage());
      return $e->getResponse()->getStatusCode();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addPage($content) {
    // The first page does not need a page break dom.
    if (count($this->pages)) {
      // Add a page break dom string.
      $this->pages[] = '<html><body><div style="page-break-before: always;"></div></body></html>';
      // Add the page html to the pages array.
      $this->pages[] = (string) $content;

    }
    else {
      // Add the page html to the pages array.
      $this->pages[] = (string) $content;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function send($filename, $force_download = TRUE) {
    try {
      $data = $this->getBlob();

      // Build up a Symfony response.
      $response = new Response($data, 200, [
        'Content-Type' => 'application/pdf',
        'Content-Transfer-Encoding' => 'Binary',
        'Content-Length' => mb_strlen($data, "8bit"),
      ]);

      // Set the response to force download or allow inline browser rendering.
      $disposition = HeaderUtils::makeDisposition(
        $force_download ? HeaderUtils::DISPOSITION_ATTACHMENT : HeaderUtils::DISPOSITION_INLINE,
        $filename,
      );
      $response->headers->set('Content-Disposition', $disposition);
      // Send it baby!
      return $response->send();
    }
    catch (PrintEngineException $e) {
      return $e;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getBlob() {
    // Prepare the HTML for printing.
    $this->prepareHtml();

    // Build up a payload.
    $payload = [
      'json' => [
        'html' => implode("", $this->pages),
        'options' => $this->buildPrintOptions(),
        'safeMode' => (bool) $this->configuration['safe_mode'],
      ],
      'gotoOptions' => [
        'waitUntil' => 'networkidle2',
      ],
    ];

    // Set the session cookie on the headless browser if needed.
    if ($session = $this->request->getSession()) {
      $payload['json']['cookies'][] = [
        'name' => $session->getName(),
        'value' => $session->getId(),
        'domain' => $this->configuration['asset_url'] ?: $this->request->getSchemeAndHttpHost(),
      ];
    }

    try {
      // Send the request to the Browserless API.
      $request = $this->httpClient->post($this->endpoint->toString(), $payload);
      // Get the PDF file blob from the response.
      return $request->getBody()->getContents();
    }
    catch (ConnectException $e) {
      // Log it.
      $this->logger->warning($e->getMessage());
      // Throw it.
      throw new PrintEngineException($e->getMessage());
    }
    catch (ClientException $e) {
      // Log it.
      $this->logger->warning($e->getMessage());
      // Throw it.
      throw new PrintEngineException($e->getMessage());
    }
    catch (ServerException $e) {
      // Log it.
      $this->logger->warning($e->getMessage());
      // Throw it.
      throw new PrintEngineException($e->getMessage());
    }
    catch (BadResponseException $e) {
      // Log it.
      $this->logger->warning($e->getMessage());
      // Throw it.
      throw new PrintEngineException($e->getMessage());
    }
  }

  /**
   * Builds a list of options to send to Browserless.
   *
   * @return array
   *   Returns an array of options.
   *
   * @see https://github.com/browserless/chrome/blob/master/src/schemas.ts#L186
   */
  private function buildPrintOptions() {
    $options = [
      'format' => $this->getPaperSizes()[$this->configuration['default_paper_size']],
      'margin' => [
        'bottom' => $this->configuration['margin_bottom'] . $this->configuration['margin_unit'],
        'left' => $this->configuration['margin_left'] . $this->configuration['margin_unit'],
        'right' => $this->configuration['margin_right'] . $this->configuration['margin_unit'],
        'top' => $this->configuration['margin_top'] . $this->configuration['margin_unit'],
      ],
      'preferCSSPageSize' => FALSE,
    ];

    if ($this->configuration['display_header_footer']) {
      $options['displayHeaderFooter'] = TRUE;

      if ($this->configuration['header_template']) {
        $options['headerTemplate'] = trim($this->configuration['header_template']);
      }

      if ($this->configuration['footer_template']) {
        $options['footerTemplate'] = trim($this->configuration['footer_template']);
      }
    }

    if ($this->configuration['print_background']) {
      $options['printBackground'] = TRUE;
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function getPaperSizes() {
    return [
      'Letter',
      'Legal',
      'Tabloid',
      'Ledger',
      'A0',
      'A1',
      'A2',
      'A3',
      'A4',
      'A5',
      'A6',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function dependenciesAvailable() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrintObject() {
    return $this->httpClient;
  }

  /**
   * Swaps out relative URLs for absolute ones.
   */
  private function prepareHtml() {
    $asset_url = $this->configuration['asset_url'] ?: $this->request->getSchemeAndHttpHost();
    foreach ($this->pages as $index => $html) {
      $dom = new \DOMDocument();
      // Suppress errors when we load the dom up.
      @$dom->loadHTML($html);

      // Load up all of the link tags.
      $link_tags = $dom->getElementsByTagName('link');

      foreach ($link_tags as $link_tag) {
        /** @var \DOMElement $link_tag */

        if ($link_tag->getNodePath() === '/html/head/link') {
          $relative_path = $link_tag->getAttribute('href');
          if ($relative_path && str_starts_with($relative_path, '/')) {
            $link_tag->setAttribute('href', $asset_url . $relative_path);
          }
        }
      }

      // Load up all of the link tags.
      $script_tags = $dom->getElementsByTagName('script');

      foreach ($script_tags as $script_tag) {
        /** @var \DOMElement $script_tag */

        $relative_path = $script_tag->getAttribute('src');

        if ($relative_path && str_starts_with($relative_path, '/')) {
          $script_tag->setAttribute('src', $asset_url . $relative_path);
        }
      }

      $this->pages[$index] = $dom->saveHTML();
    }
  }

}
